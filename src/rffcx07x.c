/*
 * rffc07x-driver: OS-independent driver for Qorvo RFFCX07X RF mixers
 *
 *  Copyright (C) 2020, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "rffcx07x.h"
#include <math.h>
#include <string.h>

#define INIT_MAGIC_VAL 0xef351a97

#define RFFCX07X_REG_LF       0x0
#define RFFCX07X_REG_XO       0x1
#define RFFCX07X_REG_CAL_TIME 0x2
#define RFFCX07X_REG_VCO_CTRL 0x3
#define RFFCX07X_REG_CT_CAL1  0x4
#define RFFCX07X_REG_CT_CAL2  0x5
#define RFFCX07X_REG_PLL_CAL1 0x6
#define RFFCX07X_REG_PLL_CAL2 0x7
#define RFFCX07X_REG_VCO_AUTO 0x8
#define RFFCX07X_REG_PLL_CTRL 0x9
#define RFFCX07X_REG_PLL_BIAS 0xA
#define RFFCX07X_REG_MIX_CONT 0xB
#define RFFCX07X_REG_P1_FREQ1 0xC
#define RFFCX07X_REG_P1_FREQ2 0xD
#define RFFCX07X_REG_P1_FREQ3 0xE
#define RFFCX07X_REG_P2_FREQ1 0xF
#define RFFCX07X_REG_P2_FREQ2 0x10
#define RFFCX07X_REG_P2_FREQ3 0x11
#define RFFCX07X_REG_FN_CTRL  0x12
#define RFFCX07X_REG_EXT_MOD  0x13
#define RFFCX07X_REG_FMOD     0x14
#define RFFCX07X_REG_SDI_CTRL 0x15
#define RFFCX07X_REG_GPO      0x16
#define RFFCX07X_REG_T_VCO    0x17
#define RFFCX07X_REG_IQMOD1   0x18
#define RFFCX07X_REG_IQMOD2   0x19
#define RFFCX07X_REG_IQMOD3   0x1A
#define RFFCX07X_REG_IQMOD4   0x1B
#define RFFCX07X_REG_T_CTRL   0x1C
#define RFFCX07X_REG_DEV_CTRL 0x1D
#define RFFCX07X_REG_TEST     0x1E
#define RFFCX07X_REG_READBACK 0x1F

#define BIT(n) (1UL << (n))

/**
 * Initializes the internal structures of the mixer
 * @param h the device handle
 * @param xtal the frequency of the reference oscillator in Hz
 * @param baudrate the baudrate of the 3-wire communication
 * @param duplex duplex mode
 * @return 0 on success or negative error code
 */
int
rffcx07x_init(struct rffcx07x *h, uint32_t xtal, uint32_t baudrate,
              rffcx07x_duplex_t duplex)
{
	if (!h) {
		return -RFFCX07X_INVAL_PARAM;
	}

	float delay = 1.0f / baudrate;

	/* Check if the clock period is within the allowed limit */
	if (delay < 40e-9f) {
		return -RFFCX07X_INVAL_PARAM;
	}
	h->delay_us  = delay * 1e6f;
	h->xtal_freq = xtal;
	switch (duplex) {
		case RFFCX07X_HD:
		case RFFCX07X_FD:
			h->duplex = duplex;
			break;
		default:
			return -RFFCX07X_INVAL_PARAM;
	}

	/* Reset the IC on a known working state */
	return rffcx07x_reset(h);
}

static int
check_dev_handle(const struct rffcx07x *h)
{
	if (!h) {
		return -RFFCX07X_INVAL_PARAM;
	}
	if (h->init != INIT_MAGIC_VAL) {
		return -RFFCX07X_NOT_READY;
	}
	return RFFCX07X_NO_ERROR;
}

/**
 * Resets the Mixer IC and re-initiates the setup of the IC
 * @param h the device handle
 * @return 0 on success or negative error code
 */
int
rffcx07x_reset(struct rffcx07x *h)
{
	if (!h) {
		return -RFFCX07X_INVAL_PARAM;
	}
	rffcx07x_set_resetx(h, 0);
	rffcx07x_delay_us(h, 10 * h->delay_us);
	rffcx07x_set_resetx(h, 1);
	rffcx07x_delay_us(h, 10 * h->delay_us);

	/* Check basic connectivity with the IC */
	int ret = rffcx07x_reg_write(h, RFFCX07X_REG_DEV_CTRL, 0x0);
	if (ret) {
		return ret;
	}
	uint16_t val = 0x0;
	ret          = rffcx07x_reg_read(h, RFFCX07X_REG_READBACK, &val);
	if (ret) {
		return ret;
	}
	switch (val & 0x3) {
		case RFFCX07X_REV_1:
		case RFFCX07X_REV_2:
			h->rev = val & 0x3;
			break;
		default:
			return -RFFCX07X_INVAL_VAL;
	}

	/* Setup device operation */
	/* VCO band select vco1 for both path1 and path2 */
	ret = rffcx07x_reg_read(h, RFFCX07X_REG_P1_FREQ1, &val);
	if (ret) {
		return ret;
	}
	val &= 0xFFFC;
	ret = rffcx07x_reg_write(h, RFFCX07X_REG_P1_FREQ1, val);
	if (ret) {
		return ret;
	}
	ret = rffcx07x_reg_read(h, RFFCX07X_REG_P2_FREQ1, &val);
	if (ret) {
		return ret;
	}
	val &= 0xFFFC;
	ret = rffcx07x_reg_write(h, RFFCX07X_REG_P2_FREQ1, val);
	if (ret) {
		return ret;
	}
	/* Set ct_min=0 and ct_max=127*/
	ret = rffcx07x_reg_read(h, RFFCX07X_REG_VCO_AUTO, &val);
	if (ret) {
		return ret;
	}
	val &= 0x1;
	val |= 127 << 8;
	/* Assume auto VCO */
	val |= 1 << 15;
	ret = rffcx07x_reg_write(h, RFFCX07X_REG_VCO_AUTO, val);
	if (ret) {
		return ret;
	}

	/* Set p1_ctv=12 */
	ret = rffcx07x_reg_read(h, RFFCX07X_REG_CT_CAL1, &val);
	if (ret) {
		return ret;
	}
	val &= 0xE0FF;
	val |= 12 << 8;
	ret = rffcx07x_reg_write(h, RFFCX07X_REG_CT_CAL1, val);
	if (ret) {
		return ret;
	}
	/* Set p2_ctv=12 */
	ret = rffcx07x_reg_read(h, RFFCX07X_REG_CT_CAL2, &val);
	if (ret) {
		return ret;
	}
	val &= 0xE0FF;
	val |= 12 << 8;
	ret = rffcx07x_reg_write(h, RFFCX07X_REG_CT_CAL2, val);
	if (ret) {
		return ret;
	}
	/* Set rgbyp=1 */
	ret = rffcx07x_reg_read(h, RFFCX07X_REG_TEST, &val);
	if (ret) {
		return ret;
	}
	val |= 1 << 2;
	ret = rffcx07x_reg_write(h, RFFCX07X_REG_TEST, val);
	if (ret) {
		return ret;
	}
	/* Set fulld */
	ret = rffcx07x_reg_read(h, RFFCX07X_REG_MIX_CONT, &val);
	if (ret) {
		return ret;
	}
	switch (h->duplex) {
		case RFFCX07X_HD:
			val &= 0x7FFF;
			break;
		case RFFCX07X_FD:
			val |= 1 << 15;
			break;
		default:
			return -RFFCX07X_INVAL_PARAM;
	}
	ret = rffcx07x_reg_write(h, RFFCX07X_REG_MIX_CONT, val);
	if (ret) {
		return ret;
	}
	if (h->rev == RFFCX07X_REV_2) {
		ret = rffcx07x_reg_read(h, RFFCX07X_REG_VCO_CTRL, &val);
		if (ret) {
			return ret;
		}
		/* Set icpup = 3 */
		val |= 0x3 << 1;
		ret = rffcx07x_reg_write(h, RFFCX07X_REG_VCO_CTRL, val);
		if (ret) {
			return ret;
		}
		ret = rffcx07x_reg_read(h, RFFCX07X_REG_PLL_CTRL, &val);
		if (ret) {
			return ret;
		}
		/* Set ldlev = 1 */
		val |= 1 << 4;
		/* Enable lock detector */
		val |= 1 << 5;
		/* Ensure automatic VCO selection */
		val &= 0xFFFB;
		ret = rffcx07x_reg_write(h, RFFCX07X_REG_PLL_CTRL, val);
		if (ret) {
			return ret;
		}
	}
	h->init = INIT_MAGIC_VAL;
	return RFFCX07X_NO_ERROR;
}

/**
 * Controls the state of the ENX pin
 * @param enable set to 1 to set the ENX pin to high, 0 to set it to low
 * @return 0 on success or negative error code
 */
__attribute__((weak)) int
rffcx07x_set_enx(struct rffcx07x *h, uint8_t enable)
{
	return RFFCX07X_NO_ERROR;
}

/**
 * Controls the state of the ENBL pin
 * @param enable set to 1 to set the ENX pin to high, 0 to set it to low
 * @return 0 on success or negative error code
 */
__attribute__((weak)) int
rffcx07x_set_enbl(struct rffcx07x *h, uint8_t enable)
{
	return RFFCX07X_NO_ERROR;
}

/**
 * Controls the state of the RESETX pin
 * @param enable set to 1 to set the RESETX pin to high, 0 to set it to low
 * @return 0 on success or negative error code
 */
__attribute__((weak)) int
rffcx07x_set_resetx(struct rffcx07x *h, uint8_t enable)
{
	return RFFCX07X_NO_ERROR;
}

/**
 * Controls the state of the MODE pin
 * @param enable set to 1 to set the MODE pin to high, 0 to set it to low
 * @return 0 on success or negative error code
 */
__attribute__((weak)) int
rffcx07x_set_mode(struct rffcx07x *h, uint8_t enable)
{
	return RFFCX07X_NO_ERROR;
}

/**
 * Controls the state of the CLK pin
 * @param enable set to 1 to set the CLK pin to high, 0 to set it to low
 * @return 0 on success or negative error code
 */
__attribute__((weak)) int
rffcx07x_set_clk(struct rffcx07x *h, uint8_t enable)
{
	return RFFCX07X_NO_ERROR;
}

/**
 * Controls the state of the SDA pin
 * @param enable set to 1 to set the SDA pin to high, 0 to set it to low
 * @return 0 on success or negative error code
 */
__attribute__((weak)) int
rffcx07x_set_sda(struct rffcx07x *h, uint8_t enable)
{
	return RFFCX07X_NO_ERROR;
}

/**
 * Gets the state of the SDA pin
 * @note before calling this function, caller should ensure that the SDA
 * pin is set in RX mode, using the rffcx07x_set_sda_dir()
 * @return 0 if the state of the SDA is low, 1 if is high or negative error code
 */
__attribute__((weak)) int
rffcx07x_get_sda(struct rffcx07x *h)
{
	return -RFFCX07X_NOT_IMPL;
}

/**
 * Changes the direction of the SDA pin when used in 3-wire mode
 * @param dir set to RFFCX07X_SDA_TX for TX operation (MCU->IC) or
 * RFFCX07X_SDA_RX for RX operation (IC->MCU)
 * @return 0 on success or negative error code
 */
__attribute__((weak)) int
rffcx07x_set_sda_dir(struct rffcx07x *h, rffcx07x_sda_dir_t dir)
{
	return RFFCX07X_NO_ERROR;
}

/**
 * Enable/disable system wide IRQs. The default implementation has no effect
 * @param enable 1 to enable the system wide IRQs, 0 to disable
 */
__attribute__((weak)) void
rffcx07x_en_irq(struct rffcx07x *h, uint8_t enable)
{
	return;
}

/**
 * Delays the execution by \p us microseconds
 * @param us the delay in microseconds
 */
__attribute__((weak)) void
rffcx07x_delay_us(struct rffcx07x *h, uint32_t us)
{
	return;
}

/**
 * Advances the clock by one cycle
 * @param h the device handle
 */
static void
advance_clk(struct rffcx07x *h)
{
	rffcx07x_delay_us(h, h->delay_us);
	rffcx07x_set_clk(h, 1);
	rffcx07x_delay_us(h, h->delay_us);
	rffcx07x_set_clk(h, 0);
}

/**
 * Writes a register using the 3-wire interface
 * @note The default implementation uses bit-banking to properly communication
 * with the IC. Depending on the MCU, user may re-implement this function and
 * use an available peripheral
 *
 * @param h the device handle
 * @param reg the register address
 * @param val the value to write
 * @return 0 on success or negative error code
 */
__attribute__((weak)) int
rffcx07x_reg_write(struct rffcx07x *h, uint8_t reg, uint16_t val)
{
	const uint8_t r = 0x7F & reg;
	if (!h) {
		return -RFFCX07X_INVAL_PARAM;
	}

	rffcx07x_en_irq(h, 0);
	rffcx07x_set_sda_dir(h, RFFCX07X_SDA_TX);
	/*
	 * One clock cycle is needed before the ENX is set to low. This is
	 * a quite weird and poorly documented requirement.
	 * Special thanks to Michael Ossmann (mike@ossmann.com) for spotting
	 * this
	 */
	advance_clk(h);
	rffcx07x_set_enx(h, 0);
	/* First bit is undefined */
	advance_clk(h);
	rffcx07x_set_sda(h, r >> 7);
	advance_clk(h);
	rffcx07x_set_sda(h, (r >> 6) & 0x1);
	advance_clk(h);
	rffcx07x_set_sda(h, (r >> 5) & 0x1);
	advance_clk(h);
	rffcx07x_set_sda(h, (r >> 4) & 0x1);
	advance_clk(h);
	rffcx07x_set_sda(h, (r >> 3) & 0x1);
	advance_clk(h);
	rffcx07x_set_sda(h, (r >> 2) & 0x1);
	advance_clk(h);
	rffcx07x_set_sda(h, (r >> 1) & 0x1);
	advance_clk(h);
	rffcx07x_set_sda(h, r & 0x1);
	advance_clk(h);
	for (uint8_t i = 0; i < 16; i++) {
		rffcx07x_set_sda(h, (val >> (15 - i)) & 0x1);
		advance_clk(h);
	}
	rffcx07x_set_enx(h, 1);
	/* Again, a cycle is needed after the ENX is set to high */
	advance_clk(h);
	rffcx07x_en_irq(h, 1);
	return RFFCX07X_NO_ERROR;
}

/**
 * Writes a register using the 3-wire interface
 * @note The default implementation uses bit-banking to properly communication
 * with the IC. Depending on the MCU, user may re-implement this function and
 * use an available peripheral
 *
 * @param h the device handle
 * @param reg the register address
 * @param val pointer to store the result of the value read
 * @return 0 on success or negative error code
 */
__attribute__((weak)) int
rffcx07x_reg_read(struct rffcx07x *h, uint8_t reg, uint16_t *val)
{
	uint16_t      res = 0;
	const uint8_t r   = (1 << 7) | reg;
	if (!h) {
		return -RFFCX07X_INVAL_PARAM;
	}

	rffcx07x_en_irq(h, 0);
	rffcx07x_set_sda_dir(h, RFFCX07X_SDA_TX);
	/*
	 * One clock cycle is needed before the ENX is set to low. This is
	 * a quite weird and poorly documented requirement.
	 * Special thanks to Michael Ossmann (mike@ossmann.com) for spotting
	 * this
	 */
	advance_clk(h);
	rffcx07x_set_enx(h, 0);
	/* First bit is undefined */
	advance_clk(h);
	rffcx07x_set_sda(h, r >> 7);
	advance_clk(h);
	rffcx07x_set_sda(h, (r >> 6) & 0x1);
	advance_clk(h);
	rffcx07x_set_sda(h, (r >> 5) & 0x1);
	advance_clk(h);
	rffcx07x_set_sda(h, (r >> 4) & 0x1);
	advance_clk(h);
	rffcx07x_set_sda(h, (r >> 3) & 0x1);
	advance_clk(h);
	rffcx07x_set_sda(h, (r >> 2) & 0x1);
	advance_clk(h);
	rffcx07x_set_sda(h, (r >> 1) & 0x1);
	advance_clk(h);
	rffcx07x_set_sda(h, r & 0x1);
	advance_clk(h);
	rffcx07x_set_sda_dir(h, RFFCX07X_SDA_RX);
	advance_clk(h);
	advance_clk(h);
	for (uint8_t i = 0; i < 16; i++) {
		int ret = rffcx07x_get_sda(h);
		if (ret < 0) {
			rffcx07x_set_enx(h, 1);
			rffcx07x_en_irq(h, 1);
			return ret;
		}
		res <<= 1;
		res |= (ret & 0x1);
		advance_clk(h);
	}
	rffcx07x_set_enx(h, 1);
	/* Again, a cycle is needed after the ENX is set to high */
	advance_clk(h);
	rffcx07x_en_irq(h, 1);
	*val = res;
	return RFFCX07X_NO_ERROR;
}

/**
 * Sets the GPO4 to output the LOCK signal or not
 * @param h the device handle
 * @param en set to 1 to enable the LOCK signal to GPO4, 0 to disable
 * @return 0 on success or negative error code
 */
int
rffcx07x_set_lock_gpo(struct rffcx07x *h, uint8_t en)
{
	int ret = check_dev_handle(h);
	if (ret) {
		return ret;
	}
	uint16_t val = 0;
	ret          = rffcx07x_reg_read(h, RFFCX07X_REG_GPO, &val);
	if (ret) {
		return ret;
	}
	val &= 0xFFFE;
	val |= (en & 0x1);
	return rffcx07x_reg_write(h, RFFCX07X_REG_GPO, val);
}
/**
 * Get the lock state
 * @param h the device handle
 * @param lock pointer to store the result. The result will be 1 if the PLL is locked,
 * is 0 the PLL is not locked
 * @return 0 on success or negative error code
 */
int
rffcx07x_get_lock(struct rffcx07x *h, uint8_t *lock)
{
	if (!lock) {
		return -RFFCX07X_INVAL_PARAM;
	}

	uint16_t val = 0;
	int      ret = rffcx07x_get_readback(h, 0x1, &val);
	if (ret) {
		return ret;
	}
	*lock = (val >> 15) & 0x1;
	return RFFCX07X_NO_ERROR;
}

/**
 * @brief Gets the calibration flag
 *
 * @param h the device handle
 * @param flag pointer to store the result. The result will be 1 if the calibration failed,
 * 0 otherwise
 * @return 0 on success or negative error code
 */
int
rffcx07x_get_calibration_flag(struct rffcx07x *h, uint8_t *flag)
{
	if (!flag) {
		return -RFFCX07X_INVAL_PARAM;
	}

	uint16_t val = 0;
	int      ret = rffcx07x_get_readback(h, 0x1, &val);
	if (ret) {
		return ret;
	}
	*flag = (val >> 1) & 0x1;
	return RFFCX07X_NO_ERROR;
}

/**
 * @brief Gets the FSM state
 *
 * @param h the device handle
 * @param state pointer to store the result
 * @return 0 on success or negative error code
 */
int
rffcx07x_get_fsm_state(struct rffcx07x *h, uint8_t *state)
{
	if (!state) {
		return -RFFCX07X_INVAL_PARAM;
	}

	uint16_t val = 0;
	int      ret = rffcx07x_get_readback(h, 0x3, &val);
	if (ret) {
		return ret;
	}
	*state = (val >> 11) & 0x1F;
	return RFFCX07X_NO_ERROR;
}

/**
 * @brief Gets the frequency flag indicating error
 *
 * @param h the device handle
 * @param flag pointer to store the result
 * @return 0 on success or negative error code
 */
int
rffcx07x_get_freq_flag(struct rffcx07x *h, uint8_t *flag)
{
	if (!flag) {
		return -RFFCX07X_INVAL_PARAM;
	}

	uint16_t val = 0;
	int      ret = rffcx07x_get_readback(h, 0x3, &val);
	if (ret) {
		return ret;
	}
	*flag = (val >> 9) & 0x3;
	return RFFCX07X_NO_ERROR;
}

/**
 * @brief Sets the readsel on the DEV_CTRL register and gets the result from
 * the READBACK register
 *
 * @param h the device handle
 * @param readsel the readsel value
 * @param val pointer to store the result of the value read
 * @return 0 on success or negative error code
 */
int
rffcx07x_get_readback(struct rffcx07x *h, uint8_t readsel, uint16_t *val)
{
	if (!val) {
		return -RFFCX07X_INVAL_PARAM;
	}

	int ret = check_dev_handle(h);
	if (ret) {
		return ret;
	}

	uint16_t tmp = 0;
	ret          = rffcx07x_reg_read(h, RFFCX07X_REG_DEV_CTRL, &tmp);
	if (ret) {
		return ret;
	}
	tmp &= 0xFFF;
	tmp |= ((readsel & 0xF) << 12);
	ret = rffcx07x_reg_write(h, RFFCX07X_REG_DEV_CTRL, tmp);
	if (ret) {
		return ret;
	}
	ret = rffcx07x_reg_read(h, RFFCX07X_REG_READBACK, val);
	if (ret) {
		return ret;
	}
	return RFFCX07X_NO_ERROR;
}

/**
 * Set gate
 * @param h the device handle
 * @param en If 0 GPO's are ANDed with enable (forced to zero
 * when the enable is low, if 1 the GPO's are available
 * when enable is low
 * @return 0 on success or negative error code
 */
int
rffcx07x_set_gate(struct rffcx07x *h, uint8_t en)
{
	int ret = check_dev_handle(h);
	if (ret) {
		return ret;
	}
	uint16_t val = 0;
	ret          = rffcx07x_reg_read(h, RFFCX07X_REG_GPO, &val);
	if (ret) {
		return ret;
	}
	val &= 0x2;
	val |= (en & 0x1) << 1;
	return rffcx07x_reg_write(h, RFFCX07X_REG_GPO, val);
}

/**
 * Sets the GPO to outputs
 * @param h the device handle
 * @param en set to 1 to enable the GPOn, 0 to disable
 * @param n number of GPO
 * @return 0 on success or negative error code
 */
int
rffcx07x_set_gpo(struct rffcx07x *h, uint8_t en, uint8_t n)
{
	int ret = check_dev_handle(h);
	if (ret) {
		return ret;
	}
	uint16_t val = 0;
	ret          = rffcx07x_reg_read(h, RFFCX07X_REG_GPO, &val);
	if (ret) {
		return ret;
	}
	if (n == 1) {
		val |= (en & 0x1) << 2;
		val |= (en & 0x1) << 9;
	} else if (n == 2) {
		val |= (en & 0x1) << 3;
		val |= (en & 0x1) << 10;
	} else if (n == 3) {
		val |= (en & 0x1) << 4;
		val |= (en & 0x1) << 11;
	} else if (n == 4) {
		val |= (en & 0x1) << 5;
		val |= (en & 0x1) << 12;
	} else if (n == 5) {
		val |= (en & 0x1) << 6;
		val |= (en & 0x1) << 13;
	} else if (n == 6) {
		val |= (en & 0x1) << 7;
		val |= (en & 0x1) << 14;
	} else
		return -RFFCX07X_INVAL_PARAM;
	return rffcx07x_reg_write(h, RFFCX07X_REG_GPO, val);
}

/**
 * Puts the LO to the output of mixer
 * @param h the device handle
 * @param en set to 1 to put LO in the mixer output, 0 to disable
 * @return 0 on success or negative error code
 */
int
rffcx07x_set_mixer_bypass(struct rffcx07x *h, uint8_t en)
{
	int ret = check_dev_handle(h);
	if (ret) {
		return ret;
	}
	uint16_t val = 0;
	ret          = rffcx07x_reg_read(h, RFFCX07X_REG_DEV_CTRL, &val);
	if (ret) {
		return ret;
	}

	val |= ((en & 0x1) << 1);
	return rffcx07x_reg_write(h, RFFCX07X_REG_DEV_CTRL, val);
}

/**
 * Sets mixer current
 * @param h the device handle
 * @param mix_current set to 0 to 7 to select mix current
 * @param path the path of the mixer
 * @return 0 on success or negative error code
 */
int
rffcx07x_set_mix_current(struct rffcx07x *h, uint16_t mix_current,
                         rffcx07x_path_t path)
{
	int ret = check_dev_handle(h);
	if (ret) {
		return ret;
	}
	if (mix_current > 7) {
		return -RFFCX07X_INVAL_PARAM;
	}
	uint16_t val = 0;
	switch (path) {
		case RFFCX07X_PATH1:
			ret = rffcx07x_reg_read(h, RFFCX07X_REG_MIX_CONT, &val);
			if (ret) {
				return ret;
			}
			val &= 0x8FFF;
			val |= (mix_current << 12);
			ret = rffcx07x_reg_write(h, RFFCX07X_REG_MIX_CONT, val);
			if (ret) {
				return ret;
			}
			break;
		case RFFCX07X_PATH2:
			ret = rffcx07x_reg_read(h, RFFCX07X_REG_MIX_CONT, &val);
			if (ret) {
				return ret;
			}
			val &= 0xF1FF;
			val |= (mix_current << 9);
			ret = rffcx07x_reg_write(h, RFFCX07X_REG_MIX_CONT, val);
			if (ret) {
				return ret;
			}
			break;
		default:
			return -RFFCX07X_INVAL_PARAM;
	}
	return RFFCX07X_NO_ERROR;
}

/**
 * Sets the frequency of the RF mixer.
 * @note This function assumes automatic VCO selection and auto CT calibration
 * @param h the device handle
 * @param freq the mixer frequency in Hz
 * @param path the path of the mixer
 * @return 0 on success or negative error code
 */
int
rffcx07x_set_freq(struct rffcx07x *h, uint64_t freq, rffcx07x_path_t path)
{
	int ret = check_dev_handle(h);
	if (ret) {
		return ret;
	}
	uint16_t       val      = 0;
	const uint16_t n_lo     = log2f(5.4e9 / freq);
	const uint16_t lodiv    = 1 << n_lo;
	const float    fvco = lodiv * freq;
	uint16_t       fbkdiv   = 2;
	/* Change prescaler and charge pump in case VCO > 3.2 GHz */
	ret    = rffcx07x_reg_read(h, RFFCX07X_REG_LF, &val);
	if (ret) {
		return ret;
	}
	if (fvco > 3.2e9f) {
		fbkdiv = 4;
		/* Set charge pump leakage to 3 for the CT_cal to work correctly */
		val |= 0x3;
	} else {
		val &= 0xFFFFC;
		val |= 0x2;
	}
	ret = rffcx07x_reg_write(h, RFFCX07X_REG_LF, val);
	if (ret) {
		return ret;
	}

	const float    n_div  = fvco / fbkdiv / h->xtal_freq;
	const uint16_t n      = n_div;
	const uint16_t nummsb = powf(2.0f, 16.0f) * (n_div - n);
	const uint16_t numlsb = (powf(2.0f, 16.0f) * (n_div - n) - nummsb);


	switch (path) {
		case RFFCX07X_PATH1:
			ret    = rffcx07x_reg_read(h, RFFCX07X_REG_P1_FREQ1, &val);
			if (ret) {
				return ret;
			}
			/* Keep the current VCO selection */
			val &= 0x3;
			val |= (n << 7) | (n_lo << 4) | ((fbkdiv / 2) << 2);

			ret = rffcx07x_reg_write(h, RFFCX07X_REG_P1_FREQ1, val);
			if (ret) {
				return ret;
			}
			ret = rffcx07x_reg_write(h, RFFCX07X_REG_P1_FREQ2, nummsb);
			if (ret) {
				return ret;
			}
			ret = rffcx07x_reg_write(h, RFFCX07X_REG_P1_FREQ3, numlsb << 8);
			if (ret) {
				return ret;
			}
			break;
		case RFFCX07X_PATH2:
			ret    = rffcx07x_reg_read(h, RFFCX07X_REG_P2_FREQ1, &val);
			if (ret) {
				return ret;
			}
			/* Keep the current VCO selection */
			val &= 0x3;
			val |= (n << 7) | (n_lo << 4) | ((fbkdiv / 2) << 2);

			ret = rffcx07x_reg_write(h, RFFCX07X_REG_P2_FREQ1, val);
			if (ret) {
				return ret;
			}
			ret = rffcx07x_reg_write(h, RFFCX07X_REG_P2_FREQ2, nummsb);
			if (ret) {
				return ret;
			}
			ret = rffcx07x_reg_write(h, RFFCX07X_REG_P2_FREQ3, numlsb << 8);
			if (ret) {
				return ret;
			}
			break;
		default:
			return -RFFCX07X_INVAL_PARAM;
	}
	return RFFCX07X_NO_ERROR;
}

/**
 * Enable/disable the corresponding mixer path
 * @param h
 * @param enable set 1 to enable the path or 0 to disable
 * @param path the desired path
 * @param pbus set to 1 to use the programming bus for setting the ENBL amd MODE
 * signals or 0 to use the corresponding GPIOs
 * @return 0 on success or negative error code
 */
int
rffcx07x_enable(struct rffcx07x *h, uint8_t enable, rffcx07x_path_t path,
                uint8_t pbus)
{
	int ret = check_dev_handle(h);
	if (ret) {
		return ret;
	}
	uint16_t val = 0;
	ret          = rffcx07x_reg_read(h, RFFCX07X_REG_SDI_CTRL, &val);
	if (ret) {
		return ret;
	}
	/* Clear sipin, enbl and mode */
	val &= 0x1FFF;
	if (pbus) {
		switch (path) {
			case RFFCX07X_PATH1:
				val |= (1 << 15) | ((enable & 0x1) << 14);
				break;
			case RFFCX07X_PATH2:
				val |= (1 << 15) | ((enable & 0x1) << 14) | (1 << 13);
				break;
			default:
				return -RFFCX07X_INVAL_PARAM;
		}
		ret = rffcx07x_reg_write(h, RFFCX07X_REG_SDI_CTRL, val);
		if (ret) {
			return ret;
		}
	} else {
		/* Make sure that the mixer is controlled from the GPIO */
		ret = rffcx07x_reg_write(h, RFFCX07X_REG_SDI_CTRL, val);
		if (ret) {
			return ret;
		}
		uint8_t mode = 0;
		switch (path) {
			case RFFCX07X_PATH1:
				mode = 0;
				break;
			case RFFCX07X_PATH2:
				mode = 1;
				break;
			default:
				return -RFFCX07X_INVAL_PARAM;
		}
		ret = rffcx07x_set_mode(h, mode & 0x1);
		if (ret) {
			return ret;
		}
		ret = rffcx07x_set_enbl(h, enable & 0x1);
		if (ret) {
			return ret;
		}
	}
	return RFFCX07X_NO_ERROR;
}


int
rffcx07x_calibrate(struct rffcx07x *h)
{
	int ret = check_dev_handle(h);
	if (ret) {
		return ret;
	}
	uint16_t val = BIT(15) | (31 << 5);
	ret = rffcx07x_reg_write(h, RFFCX07X_REG_CAL_TIME, val);
	if (ret) {
		return ret;
	}

	ret = rffcx07x_reg_read(h, RFFCX07X_REG_VCO_CTRL, &val);
	if (ret) {
		return ret;
	}
	/* Increase the averaging samples for the calibration */
	val |= 0x66;
	ret = rffcx07x_reg_write(h, RFFCX07X_REG_VCO_CTRL, val);
	if (ret) {
		return ret;
	}

	/* Wait for the calibration to finalize before the RF port is available */
	val = BIT(15) | 0x28;
	ret = rffcx07x_reg_write(h, RFFCX07X_REG_PLL_CAL1, val);
	if (ret) {
		return ret;
	}
	ret = rffcx07x_reg_write(h, RFFCX07X_REG_PLL_CAL2, val);
	if (ret) {
		return ret;
	}
	return RFFCX07X_NO_ERROR;
}