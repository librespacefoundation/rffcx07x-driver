/*
 * rffc07x-driver: OS-independent driver for Qorvo RFFCX07X RF mixers
 *
 *  Copyright (C) 2020, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef INCLUDE_RFFCX07X_H_
#define INCLUDE_RFFCX07X_H_

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef enum {
	RFFCX07X_NO_ERROR = 0,
	RFFCX07X_INVAL_PARAM,
	RFFCX07X_NOT_IMPL,
	RFFCX07X_INVAL_VAL,
	RFFCX07X_NOT_READY
} rffcx07x_error_t;

typedef enum {
	RFFCX07X_SDA_TX,
	RFFCX07X_SDA_RX
} rffcx07x_sda_dir_t;

typedef enum {
	RFFCX07X_REV_1,
	RFFCX07X_REV_2
} rffcx07x_rev_t;

/**
 * Duplex mode
 */
typedef enum {
	RFFCX07X_HD, //!< Half duplex
	RFFCX07X_FD  //!< Full duplex
} rffcx07x_duplex_t;

typedef enum {
	RFFCX07X_PATH1,
	RFFCX07X_PATH2
} rffcx07x_path_t;

struct rffcx07x {
	uint32_t          init;
	uint32_t          delay_us;
	rffcx07x_duplex_t duplex;
	rffcx07x_rev_t    rev;
	uint32_t          xtal_freq;
	void              *mode_gpio_dev;
	void              *enbl_gpio_dev;
	void              *rst_gpio_dev;
	void              *enx_gpio_dev;
	void              *sda_gpio_dev;
	void              *clk_dev;
	void              *user_dev0;
	void              *user_dev1;
};

int
rffcx07x_init(struct rffcx07x *h, uint32_t xtal, uint32_t baudrate,
              rffcx07x_duplex_t duplex);

int
rffcx07x_reset(struct rffcx07x *h);

int
rffcx07x_set_enx(struct rffcx07x *h, uint8_t enable);

int
rffcx07x_set_enbl(struct rffcx07x *h, uint8_t enable);

int
rffcx07x_set_resetx(struct rffcx07x *h, uint8_t enable);

int
rffcx07x_set_mode(struct rffcx07x *h, uint8_t enable);

int
rffcx07x_set_clk(struct rffcx07x *h, uint8_t enable);

int
rffcx07x_set_sda(struct rffcx07x *h, uint8_t enable);

int
rffcx07x_get_sda(struct rffcx07x *h);

int
rffcx07x_set_sda_dir(struct rffcx07x *h, rffcx07x_sda_dir_t dir);

void
rffcx07x_en_irq(struct rffcx07x *h, uint8_t enable);

void
rffcx07x_delay_us(struct rffcx07x *h, uint32_t us);

int
rffcx07x_reg_write(struct rffcx07x *h, uint8_t reg, uint16_t val);

int
rffcx07x_reg_read(struct rffcx07x *h, uint8_t reg, uint16_t *val);

int
rffcx07x_set_lock_gpo(struct rffcx07x *h, uint8_t en);

int
rffcx07x_get_lock(struct rffcx07x *h, uint8_t *lock);

int
rffcx07x_get_calibration_flag(struct rffcx07x *h, uint8_t *flag);

int
rffcx07x_get_fsm_state(struct rffcx07x *h, uint8_t *state);

int
rffcx07x_get_freq_flag(struct rffcx07x *h, uint8_t *flag);

int
rffcx07x_get_readback(struct rffcx07x *h, uint8_t readsel, uint16_t *val);

int
rffcx07x_set_gate(struct rffcx07x *h, uint8_t en);

int
rffcx07x_set_gpo(struct rffcx07x *h, uint8_t en, uint8_t n);

int
rffcx07x_set_mixer_bypass(struct rffcx07x *h, uint8_t en);

int
rffcx07x_set_mix_current(struct rffcx07x *h, uint16_t mix_current,
                         rffcx07x_path_t path);

int
rffcx07x_set_freq(struct rffcx07x *h, uint64_t freq, rffcx07x_path_t path);

int
rffcx07x_enable(struct rffcx07x *h, uint8_t enable, rffcx07x_path_t path,
                uint8_t pbus);

int
rffcx07x_calibrate(struct rffcx07x *h);

#ifdef __cplusplus
}
#endif

#endif /* INCLUDE_RFFCX07X_H_ */
